const selectionsElement = document.querySelector('.selections')
const finalColumn = document.querySelector('[data-final-column]')
const yourScoreSpan = document.querySelector('[data-your-score]')
const computerScoreSpan = document.querySelector('[data-computer-score]')

const SELECTIONS = [
  {
    name: 'rock',
    emoji: '🪨',
    beats: ['scissors', 'lizard']
  },
  {
    name: 'paper',
    emoji: '📜',
    beats: ['rock', 'spock']
  },
  {
    name: 'scissors',
    emoji: '✂️',
    beats: ['paper', 'lizard']
  },
  {
    name: 'lizard',
    emoji: '🦎',
    beats: ['spock', 'paper']
  },
  {
    name: 'spock',
    emoji: '🖖',
    beats: ['scissors', 'rock']
  }
]


SELECTIONS.forEach(selection => {
  const selectionButton = document.createElement('button')
  selectionButton.classList.add('selection')
  selectionButton.dataset.selection = selection.name
  selectionButton.innerText = selection.emoji
  selectionButton.addEventListener('click', e => {  
    makeSelection(selection)
  })
  selectionsElement.append(selectionButton)
})

function makeSelection(selection) {
  const computerSelection = randomSelection()
  const yourWinner = isWinner(selection, computerSelection)
  const ComputerWinner = isWinner(computerSelection, selection)
  addSelectionResult(computerSelection, ComputerWinner)
  addSelectionResult(selection, yourWinner)

  if (yourWinner) incrementScore(yourScoreSpan)
  if (ComputerWinner) incrementScore(computerScoreSpan)
}

function incrementScore(scoreSpan) {
  scoreSpan.innerText = parseInt(scoreSpan.innerText) + 1
}

function addSelectionResult(selection, winner) {
  const div = document.createElement('div')
  div.innerText = selection.emoji
  div.classList.add('result-selection')
  if (winner) div.classList.add('winner')
  finalColumn.after(div)
}
 
function isWinner(selection, opponentSelection) {
  return selection.beats.includes(opponentSelection.name)
}

function randomSelection() {
  const randomIndex = Math.floor(Math.random() * SELECTIONS.length)
  return SELECTIONS[randomIndex]
}